///update_level(mdlpath,colmdlpath,sprite texture)
gml_pragma("forceinline");

var realpath = "levels/"+argument[0];
var realcolpath = "levels/"+argument[1];

global.leveltexture = argument[2];
p3dc_clear_model(global.levelcollision);
global.levelcollision = p3dc_begin_model();
p3dc_add_model(realcolpath,0,0,0);
p3dc_end_model();

d3d_model_clear(global.levelmesh);
d3d_model_load(global.levelmesh,realpath);
