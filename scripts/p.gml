///p(x,y,argb32)
gml_pragma("forceinline");
var funny = irandom_range(-16777215,16777215);
draw_set_alpha(((funny >> 24) & 255)/255);
draw_point_colour(argument[0],argument[1],make_colour_rgb((funny >> 16) & 255,(funny >> 8) & 255,funny & 255));
