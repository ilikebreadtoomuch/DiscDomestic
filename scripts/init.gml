//show_debug_overlay(1);
p3dc_init();
//randomize();
//makesprites();
show_message("wasd to move, down key to interact, left alt to cycle through modes, tab for freecam that controls like giftscop's freecam and space to toggle laggy and inaccurate and bad ntsc that is actually pal");
display_set_gui_size(320,240);
global.camx = 0;
global.camy = 0;
global.camz = 0;
global.campitch = 0;
global.camyaw = 0;
global.camfov = 39;

global.levelmesh = d3d_model_create();
global.levelcollision = p3dc_begin_model();
global.leveltexture = spr_tileset;
p3dc_add_floor(0,0,0,4096,4096,0);
p3dc_end_model();

global.controllablecollision = p3dc_begin_model();
p3dc_add_cylinder(-4,-4,0,4,4,8,1,16);//p3dc_add_block(-8,-8,0,8,8,16);
p3dc_end_model();

global.billboardable = d3d_model_create();
d3d_model_primitive_begin(global.billboardable,pr_trianglelist);
d3d_model_vertex_texture(global.billboardable,-16,-32,0,0,0);
d3d_model_vertex_texture(global.billboardable,16,-32,0,1,0);
d3d_model_vertex_texture(global.billboardable,16,0,0,1,1);
d3d_model_vertex_texture(global.billboardable,16,0,0,1,1);
d3d_model_vertex_texture(global.billboardable,-16,0,0,0,1);
d3d_model_vertex_texture(global.billboardable,-16,-32,0,0,0);
d3d_model_primitive_end(global.billboardable);

global.backsurf = surface_create(320,240);
global.foresurf = surface_create(320,240);

global.enablentsc = 0;

enum inputs {
    up,
    down,
    left,
    right,
    start,
    select,
    bt_x,
    bt_circle,
    bt_square,
    bt_triangle,
    r1,
    r2,
    l1,
    l2,
};

/*global.input = ds_map_create();
global.input[?"up"] = ds_map_create();
global.input[?"down"] = ds_map_create();
global.input[?"left"] = ds_map_create();
global.input[?"right"] = ds_map_create();
global.input[?"start"] = ds_map_create();
global.input[?"select"] = ds_map_create();
global.input[?"bt_x"] = ds_map_create();
global.input[?"bt_circle"] = ds_map_create();
global.input[?"bt_square"] = ds_map_create();
global.input[?"bt_triangle"] = ds_map_create();
global.input[?"r1"] = ds_map_create();
global.input[?"r2"] = ds_map_create();
global.input[?"l1"] = ds_map_create();
global.input[?"l2"] = ds_map_create();*/

global.time = 0;
global.roomtime = 0;

/*global.demo = 0;
global.recordinput = 0;
var sep = "_";
global.inittime = (string(current_year)+sep+string(current_month)+sep+string(current_day)+sep+string(current_hour)+sep+string(current_minute))+".ini";*/

global.inputaccept = 1;
global.freezeinput = -1;

global.loadingscreensprite = 0  ;
