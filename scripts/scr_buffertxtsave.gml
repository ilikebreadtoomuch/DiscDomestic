///scr_buffertxtsave(filename,string)
var filename = argument0;
var strign = argument1;

var buff = buffer_create(string_byte_length(strign)+1,buffer_fixed,1);
buffer_write(buff,buffer_string,strign);
buffer_save(buff,filename);
buffer_delete(buff);
