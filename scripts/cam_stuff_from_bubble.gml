///cam_stuff_from_bubble(yaw,pitch)
gml_pragma("forceinline");
var yaw = argument0;
var pitch = argument1;
var pi_180 = pi/180;

var lx = cos(yaw*pi_180)*cos(pitch*pi_180);
var ly = sin(yaw*pi_180)*cos(pitch*pi_180);
var lz = sin(pitch*pi_180);

var temparray = array_create(2);
temparray[0] = lx;
temparray[1] = ly;
temparray[2] = lz;

return temparray;
