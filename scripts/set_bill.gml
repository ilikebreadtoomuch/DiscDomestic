///set_bill(x,y,z)
gml_pragma("forceinline");
var uni = shader_get_uniform(shBillBoard,"world_pos");
shader_set_uniform_f(uni,argument0,argument1,argument2);
