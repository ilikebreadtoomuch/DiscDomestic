#define keycheck
///keycheck(input.)
///TODO: demo stuff, will probably be handled completely different in the future
gml_pragma("forceinline");
var gpsensitivity = 0.4;
var currentframe = string(global.time);
if(1) {
    switch(argument[0]) {
        case inputs.up:
            var val = (keyboard_check(ord('W')) || gamepad_button_check(0,gp_padu) || (gamepad_axis_value(0,gp_axislv) <= -gpsensitivity));
            if(global.freezeinput = inputs.up) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.down:
            var val = (keyboard_check(ord('S')) || gamepad_button_check(0,gp_padd) || (gamepad_axis_value(0,gp_axislv) >= gpsensitivity));
            if(global.freezeinput = inputs.down) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.left:
            var val = (keyboard_check(ord('A')) || gamepad_button_check(0,gp_padl) || (gamepad_axis_value(0,gp_axislh) <= -gpsensitivity));
            if(global.freezeinput = inputs.left) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.right:
            var val = (keyboard_check(ord('D')) || gamepad_button_check(0,gp_padr) || (gamepad_axis_value(0,gp_axislh) >= gpsensitivity));
            if(global.freezeinput = inputs.right) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.start:
            var val = (keyboard_check(vk_return) || gamepad_button_check(0,gp_start));
            if(global.freezeinput = inputs.start) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.select:
            var val = (keyboard_check(vk_rshift) || gamepad_button_check(0,gp_select));
            if(global.freezeinput = inputs.select) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.bt_x:
            var val = (keyboard_check(vk_down) || gamepad_button_check(0,gp_face1));
            if(global.freezeinput = inputs.bt_x) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.bt_circle:
            var val = (keyboard_check(vk_right) || gamepad_button_check(0,gp_face2));
            if(global.freezeinput = inputs.bt_circle) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.bt_square:
            var val = (keyboard_check(vk_left) || gamepad_button_check(0,gp_face3));
            if(global.freezeinput = inputs.bt_square) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.bt_triangle:
            var val = (keyboard_check(vk_up) || gamepad_button_check(0,gp_face4));
            if(global.freezeinput = inputs.bt_triangle) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.r1:
            var val = (keyboard_check(ord('R')) || gamepad_button_check(0,gp_shoulderr));
            if(global.freezeinput = inputs.r1) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.r2:
            var val = (keyboard_check(ord('T')) || gamepad_button_check(0,gp_shoulderrb));
            if(global.freezeinput = inputs.r2) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.l1:
            var val = (keyboard_check(ord('F')) || gamepad_button_check(0,gp_shoulderl));
            if(global.freezeinput = inputs.l1) {
                return 1;
            } else {
                return val;
            }
            break;
        case inputs.l2:
            var val = (keyboard_check(ord('G')) || gamepad_button_check(0,gp_shoulderlb));
            if(global.freezeinput = inputs.l2) {
                return 1;
            } else {
                return val;
            }
            break;
    }
} else if(0){
    /*switch(argument[0]) {
        case inputs.up:
            if(ds_map_exists(global.input[?"up"],currentframe)) {
                return ds_map_find_value(global.input[?"up"],currentframe);
            }
            break;
        case inputs.down:
            if(ds_map_exists(global.input[?"down"],currentframe)) {
                return ds_map_find_value(global.input[?"down"],currentframe);
            }
            break;
        case inputs.left:
            if(ds_map_exists(global.input[?"left"],currentframe)) {
                return ds_map_find_value(global.input[?"left"],currentframe);
            }
            break;
        case inputs.right:
            if(ds_map_exists(global.input[?"right"],currentframe)) {
                return ds_map_find_value(global.input[?"right"],currentframe);
            }
            break;
        case inputs.start:
            if(ds_map_exists(global.input[?"start"],currentframe)) {
                return ds_map_find_value(global.input[?"start"],currentframe);
            }
            break;
        case inputs.select:
            if(ds_map_exists(global.input[?"select"],currentframe)) {
                return ds_map_find_value(global.input[?"select"],currentframe);
            }
            break;
        case inputs.bt_x:
            if(ds_map_exists(global.input[?"bt_x"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_x"],currentframe);
            }
            break;
        case inputs.bt_circle:
            if(ds_map_exists(global.input[?"bt_circle"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_circle"],currentframe);
            }
            break;
        case inputs.bt_square:
            if(ds_map_exists(global.input[?"bt_square"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_square"],currentframe);
            }
            break;
        case inputs.bt_triangle:
            if(ds_map_exists(global.input[?"bt_triangle"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_triangle"],currentframe);
            }
            break;
        case inputs.r1:
            if(ds_map_exists(global.input[?"r1"],currentframe)) {
                return ds_map_find_value(global.input[?"r1"],currentframe);
            }
            break;
        case inputs.r2:
            if(ds_map_exists(global.input[?"r2"],currentframe)) {
                return ds_map_find_value(global.input[?"r2"],currentframe);
            }
            break;
        case inputs.l1:
            if(ds_map_exists(global.input[?"l1"],currentframe)) {
                return ds_map_find_value(global.input[?"l1"],currentframe);
            }
            break;
        case inputs.l2:
            if(ds_map_exists(global.input[?"l2"],currentframe)) {
                return ds_map_find_value(global.input[?"l2"],currentframe);
            }
            break;
    }*/
}
return 0;

#define keycheck_pressed
///keycheck_pressed(input.)
///TODO: demo stuff, will probably be handled completely different in the future
gml_pragma("forceinline");
var gpsensitivity = 0.2;
var currentframe = string(global.time);
if(1) {
    switch(argument[0]) {
        case inputs.up:
            var val = (keyboard_check_pressed(ord('W')) || gamepad_button_check_pressed(0,gp_padu));
            /*if(global.recordinput) {
                ds_map_set(global.input[?"up"],string(global.time),val);
            }*/
            return val;
            break;
        case inputs.down:
            var val = (keyboard_check_pressed(ord('S')) || gamepad_button_check_pressed(0,gp_padd));
            /*if(global.recordinput) {
                ds_map_set(global.input[?"down"],string(global.time),val);
            }*/
            return val;
            break;
        case inputs.left:
            var val = (keyboard_check_pressed(ord('A')) || gamepad_button_check_pressed(0,gp_padl));
            /*if(global.recordinput) {
                ds_map_set(global.input[?"down"],string(global.time),val);
            }*/
            return val;
            break;
        case inputs.right:
            return (keyboard_check_pressed(ord('D')) || gamepad_button_check_pressed(0,gp_padr));
            break;
        case inputs.start:
            return (keyboard_check_pressed(vk_return) || gamepad_button_check_pressed(0,gp_start));
            break;
        case inputs.select:
            return (keyboard_check_pressed(vk_rshift) || gamepad_button_check_pressed(0,gp_select));
            break;
        case inputs.bt_x:
            return (keyboard_check_pressed(vk_down) || gamepad_button_check_pressed(0,gp_face1));
            break;
        case inputs.bt_circle:
            return (keyboard_check_pressed(vk_right) || gamepad_button_check_pressed(0,gp_face2));
            break;
        case inputs.bt_square:
            return (keyboard_check_pressed(vk_left) || gamepad_button_check_pressed(0,gp_face3));
            break;
        case inputs.bt_triangle:
            return (keyboard_check_pressed(vk_up) || gamepad_button_check_pressed(0,gp_face4));
            break;
        case inputs.r1:
            return (keyboard_check_pressed(ord('R')) || gamepad_button_check_pressed(0,gp_shoulderr));
            break;
        case inputs.r2:
            return (keyboard_check_pressed(ord('T')) || gamepad_button_check_pressed(0,gp_shoulderrb));
            break;
        case inputs.l1:
            return (keyboard_check_pressed(ord('F')) || gamepad_button_check_pressed(0,gp_shoulderl));
            break;
        case inputs.l2:
            return (keyboard_check_pressed(ord('G')) || gamepad_button_check_pressed(0,gp_shoulderlb));
            break;   
    }
} else if(0){
    /*switch(argument[0]) {
        case inputs.up:
            if(ds_map_exists(global.input[?"up"],currentframe)) {
                return ds_map_find_value(global.input[?"up"],currentframe);
            }
            break;
        case inputs.down:
            if(ds_map_exists(global.input[?"down"],currentframe)) {
                return ds_map_find_value(global.input[?"down"],currentframe);
            }
            break;
        case inputs.left:
            if(ds_map_exists(global.input[?"left"],currentframe)) {
                return ds_map_find_value(global.input[?"left"],currentframe);
            }
            break;
        case inputs.right:
            if(ds_map_exists(global.input[?"right"],currentframe)) {
                return ds_map_find_value(global.input[?"right"],currentframe);
            }
            break;
        case inputs.start:
            if(ds_map_exists(global.input[?"start"],currentframe)) {
                return ds_map_find_value(global.input[?"start"],currentframe);
            }
            break;
        case inputs.select:
            if(ds_map_exists(global.input[?"select"],currentframe)) {
                return ds_map_find_value(global.input[?"select"],currentframe);
            }
            break;
        case inputs.bt_x:
            if(ds_map_exists(global.input[?"bt_x"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_x"],currentframe);
            }
            break;
        case inputs.bt_circle:
            if(ds_map_exists(global.input[?"bt_circle"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_circle"],currentframe);
            }
            break;
        case inputs.bt_square:
            if(ds_map_exists(global.input[?"bt_square"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_square"],currentframe);
            }
            break;
        case inputs.bt_triangle:
            if(ds_map_exists(global.input[?"bt_triangle"],currentframe)) {
                return ds_map_find_value(global.input[?"bt_triangle"],currentframe);
            }
            break;
        case inputs.r1:
            if(ds_map_exists(global.input[?"r1"],currentframe)) {
                return ds_map_find_value(global.input[?"r1"],currentframe);
            }
            break;
        case inputs.r2:
            if(ds_map_exists(global.input[?"r2"],currentframe)) {
                return ds_map_find_value(global.input[?"r2"],currentframe);
            }
            break;
        case inputs.l1:
            if(ds_map_exists(global.input[?"l1"],currentframe)) {
                return ds_map_find_value(global.input[?"l1"],currentframe);
            }
            break;
        case inputs.l2:
            if(ds_map_exists(global.input[?"l2"],currentframe)) {
                return ds_map_find_value(global.input[?"l2"],currentframe);
            }
            break;
    }*/
}
return 0;