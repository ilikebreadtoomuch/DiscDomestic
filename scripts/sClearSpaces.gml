///sClearSpaces(x,y)
gml_pragma("forceinline");
{
    var csX, csY, c_grid, cCheck;
    csX = argument0;
    csY = argument1;
    c_grid = ds_grid_create(ds_grid_width(game_grid),ds_grid_height(game_grid));
    //click_grid
    ds_grid_clear(c_grid,0);
    ds_grid_set(c_grid,csX,csY,1);
    
    cCheck = true;
    while(cCheck==true){
        cCheck = false
        for(var cix = 0;cix<ds_grid_width(c_grid);cix++){
            for(var ciy = 0;ciy<ds_grid_height(c_grid);ciy++){
                if(ds_grid_get(c_grid,cix,ciy)==1){
                    for(var vcx = -1;vcx <= 1;vcx ++){
                        for(var vcy = -1;vcy <= 1;vcy ++){
                            if(cix+vcx>=0 && cix+vcx<=ds_grid_width(c_grid)-1){
                                if(ciy+vcy>=0 && ciy+vcy<=ds_grid_height(c_grid)-1){
                                    if(abs(vcx)!=abs(vcy)){
                                        if(ds_grid_get(c_grid,cix+vcx,ciy+vcy)==0){
                                            if(sCheckBomb(cix+vcx,ciy+vcy)==0){
                                                ds_grid_set(c_grid,cix+vcx,ciy+vcy,1);
                                                //Do the 3x3 Click Stuff here automatically
                                                //ds_grid_set(click_grid,cix+vcx,ciy+vcy,1);
                                                cCheck = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    for(var zcix = 0;zcix<ds_grid_width(c_grid);zcix++){
        for(var zciy = 0;zciy<ds_grid_height(c_grid);zciy++){
            if(ds_grid_get(c_grid,zcix,zciy)==1){
                for(var ccvx = -1;ccvx <= 1;ccvx++){
                    for(var ccvy = -1;ccvy <= 1;ccvy++){
                        if(zcix+ccvx>=0 && zcix+ccvx<=ds_grid_width(game_grid)-1){
                            if(zciy+ccvy>=0 && zciy+ccvy<=ds_grid_height(game_grid)-1){
                                ds_grid_set(click_grid,zcix+ccvx,zciy+ccvy,1);
                            }
                        }
                    }
                }
            }
        }
    }
    ds_grid_destroy(c_grid);
}
