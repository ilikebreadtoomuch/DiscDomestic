///sCheckBomb(x,y);
gml_pragma("forceinline");
{
    var vvx, vvy, cbombs;
    vvx = argument0;
    vvy = argument1;
    cbombs = 0;
    
    for(var vx = -1;vx <= 1;vx ++){
        for(var vy = -1;vy <= 1;vy ++){
            if(vvx+vx>=0 && vvx+vx<=ds_grid_width(game_grid)-1){
                if(vvy+vy>=0 && vvy+vy<=ds_grid_height(game_grid)-1){
                    if(ds_grid_get(game_grid,vvx+vx,vvy+vy)==1){
                        cbombs++;
                    }
                }
            }
        }
    }
    
    return cbombs;
}
