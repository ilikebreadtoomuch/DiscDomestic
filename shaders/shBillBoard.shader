attribute vec3 in_Position; 
attribute vec2 in_TextureCoord;
attribute vec4 in_Colour;

varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vPos;

uniform vec3 world_pos;

void main()
{
    vec4 world_view_position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * vec4(world_pos,1.0);
    //gl_Position = world_view_position + vec4(in_Position.x * 1050.0/1680.0,-in_Position.y, 0.0, 0.0);
    //gl_Position = world_view_position + vec4(in_Position.x * 240.0/320.0,-in_Position.y, 0.0, 0.0);
    gl_Position = world_view_position + vec4(in_Position.x * 240.0/320.0,-in_Position.y, 0.0, 0.0);
    //gl_Position.z = floor(gl_Position.z * 8.0) / 8.0;
    v_vTexcoord = in_TextureCoord;
    v_vColour = in_Colour;
    v_vPos = gl_Position;
}
//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vPos;

void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
    //gl_FragColor.xyz = vec3(v_vPos.z,v_vPos.z,v_vPos.z) / 256.0;
}

