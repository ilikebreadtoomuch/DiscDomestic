//
// Simple passthrough vertex shader
//
attribute vec3 in_Position;                  // (x,y,z)
//attribute vec3 in_Normal;                  // (x,y,z)     unused in this shader.
attribute vec4 in_Colour;                    // (r,g,b,a)
attribute vec2 in_TextureCoord;              // (u,v)

varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vPos;

void main()
{
    vec4 object_space_pos = vec4( in_Position.x, in_Position.y, in_Position.z, 1.0);
    gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    
    v_vColour = in_Colour;
    v_vTexcoord = in_TextureCoord;
    v_vPos = gl_Position;
}

//######################_==_YOYO_SHADER_MARKER_==_######################@~//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
varying vec4 v_vPos;

uniform mediump int FrameDirection;
uniform mediump int FrameCount;
uniform mediump vec2 OutputSize;
uniform mediump vec2 TextureSize;
uniform mediump vec2 InputSize;
uniform sampler2D Texture;
uniform sampler2D nes_lut;

#define float2 vec2
#define float3 vec3
#define float4 vec4
#define frac(c) fract(c)
#define saturate(c) clamp(c, 0.0, 1.0)
#define fmod(x,y) mod(x,y)
#define mul(x,y) (y*x)
#define float2x2 mat2
#define float3x3 mat3
#define float4x4 mat4
#define bool2 bvec2
#define bool3 bvec3
#define bool4 bvec4
#define static

#undef PARAMETER_UNIFORM
#define USE_DELAY_LINE
#define COMPENSATE_WIDTH
#define USE_COLORIMETRY
#define USE_CORE_SIZE
//#define USE_RAW
//#define USE_LUT
//#define ANIMATE_PHASE

precision highp float;

/*float mod(a,b) {
    return a - (b * ~~(a/b));
}*/

#define Source Texture

#define SourceSize vec4(TextureSize, 1.0 / TextureSize) //either TextureSize or InputSize
#define OutSize vec4(OutputSize, 1.0 / OutputSize)

#ifdef PARAMETER_UNIFORM

#else

#define Brightness Brightness_static
#define Gamma Gamma_static

static const int
Ywidth_static = 12,
Uwidth_static = 23,
Vwidth_static = 23;

#define Ywidth Ywidth_static
#define Uwidth Uwidth_static
#define Vwidth Vwidth_static

static const int Mwidth = 23;//max(float(Ywidth), max(float(Uwidth), float(Vwidth)));

#ifdef USE_CORE_SIZE
// just use core output size.
#define size (InputSize.xy)
#else
static const int SizeX = 256;
static const int SizeY = 240;
static const float2 size = float2(SizeX,SizeY);
#endif

#endif

#ifndef PARAMETER_UNIFORM

// NTSC standard gamma = 2.2
// PAL standard gamma = 2.8
// according to many sources, very unlikely gamma of TV is 2.8
// most likely gamma of PAL TV is in range 2.4-2.5
static const float Gamma_static = 2.5; // gamma of virtual TV

static const float Brightness_static = 0.0;
static const float Contrast_static = 1.0;
static const float Saturation_static = 1.0;

// correct one is -2.5
// works only with USE_RAW
static const float HueShift = -2.5;

// rotation of hue due to luma level.
static const float HueRotation = 2.;

// touch this only if you know what you doing
static const float Phase_Y = 2.; // fmod(341*10,12)
static const float Phase_One = 0.; // alternating phases.
static const float Phase_Two = 8.;

// screen size, scanlines = y*2; y one field, and y other field.

// count of pixels of virtual TV.
// value close to 1000 produce small artifacts
static const int TV_Pixels = 400;

static const float dark_scanline = 0.5; // half

#endif

static const mat3 RGB_to_XYZ =
mat3(
0.4306190, 0.3415419, 0.1783091,
0.2220379, 0.7066384, 0.0713236,
0.0201853, 0.1295504, 0.9390944
);

static const mat3 XYZ_to_sRGB =
mat3(
 3.2406, -1.5372, -0.4986,
-0.9689,  1.8758,  0.0415,
 0.0557, -0.2040,  1.0570
);

static const float YUV_u = 0.492;
static const float YUV_v = 0.877;

static const mat3 RGB_to_YUV =
mat3(
float3( 0.299, 0.587, 0.114), //Y
float3(-0.299,-0.587, 0.886)*YUV_u, //B-Y
float3( 0.701,-0.587,-0.114)*YUV_v //R-Y
);

#ifdef USE_RAW
#ifndef USE_LUT
static const float Voltage_0 = 0.518;
static const float Voltage_1 = 1.962;
static const float DeltaV = (Voltage_1-Voltage_0);
#else
static const float Voltage_0 = 0.15103768593097774;
static const float Voltage_1 = 1.;
static const float DeltaV = (Voltage_1-Voltage_0);
#endif

#else
static const float DeltaV = 1.;
#endif

#ifdef USE_DELAY_LINE
static const float comb_line = 1.;
#else
static const float comb_line = 2.;
#endif

static float RGB_y = Contrast_static/float(Ywidth_static)/DeltaV;
static float RGB_u = comb_line*Contrast_static*Saturation_static/YUV_u/float(Uwidth_static)/DeltaV;
static float RGB_v = comb_line*Contrast_static*Saturation_static/YUV_v/float(Vwidth_static)/DeltaV;

static const float pi = 3.1415926535897932384626433832795;

#ifdef USE_RAW

bool InColorPhase(int color, float phase)
{
return fmod((float(color)*2. + phase),24.) < 12.;
}

// signal low
const float levels_0 = 0.350;
const float levels_1 = 0.518;
const float levels_2 = 0.962;
const float levels_3 = 1.550;
// signal high
const float levels_4 = 1.094;
const float levels_5 = 1.506;
const float levels_6 = 1.962;
const float levels_7 = 1.962;

#ifndef USE_LUT
// from nesdev wiki page NTSC_video
float NTSCsignal(float3 pixel, float phase)
{
// Voltage levels, relative to synch voltage
static const float black=.518, white=1.962, attenuation=.746;

// Decode the NES color.
int color = int(pixel.r*15.);// 0..15 "cccc"
int level = int(pixel.g*3.);// 0..3  "ll"
int emphasis = int(pixel.b*7.+0.1);// 0..7  "eee"
if (color > 13) { level = 1; }// For colors 14..15, level 1 is forced.

// The square wave for this color alternates between these two voltages:
float low = levels_0, high = levels_4;
if (level == 1) { low = levels_1, high = levels_5; }
if (level == 2) { low = levels_2, high = levels_6; }
if (level == 3) { low = levels_3, high = levels_7; }
if(color == 0) { low = high; } // For color 0, only high level is emitted
if(color > 12) { high = low; } // For colors 13..15, only low level is emitted


// Generate the square wave
// When de-emphasis bits are set, some parts of the signal are attenuated:
float2 e = fmod(float2(emphasis,emphasis), float2(2.,4.));
float signal = InColorPhase(color,phase) ? high : low;

if( ((int(e.x) != 0) && InColorPhase(0,phase))
||  ((int(e.y-e.x) != 0) && InColorPhase(4,phase))
||  ((emphasis-int(e.y) != 0) && InColorPhase(8,phase)) )
return signal * attenuation;
else
return signal;
}

#else
float NTSCsignal(float3 pixel, float phase)
{
return texture2D(nes_lut,float2(dot(pixel,float3(
15.*(8.)/512.,
3.*(16.*8.)/512.,
7./512.)
) + 0.5/(4.*16.*8.), frac(phase/24.))).r;
}

#endif

#endif

float sinn(float x)
{
return sin(/*fmod(x,24)*/x*(pi*2./24.));
}

float coss(float x)
{
return cos(/*fmod(x,24)*/x*(pi*2./24.));
}

float3 monitor(sampler2D tex, float2 p)
{
mat3 YUV_to_RGB = mat3(
float3(1., 1., 1.)*RGB_y,
float3(0., -0.114/0.587, 1.)*RGB_u,
float3(1., -0.299/0.587, 0.)*RGB_v
);
#ifdef PARAMETER_UNIFORM
float2 size = float2(SizeX,SizeY);
#endif
// align vertical coord to center of texel
float2 uv = float2(
#ifdef COMPENSATE_WIDTH
p.x+p.x*(12.0/8.0)/size.x,
#else
p.x,
#endif
(floor(p.y*TextureSize.y)+0.5)/TextureSize.y);
#ifdef USE_DELAY_LINE
float2 sh = (InputSize/TextureSize/size)*float2(14./10.,-1.0);
#endif
float2 pc = uv*TextureSize/InputSize*size*float2(10.,1.);
float alpha = dot(floor(float2(pc.x,pc.y)),float2(2.,Phase_Y*2.));
alpha += Phase_One*2.;
#ifdef ANIMATE_PHASE
if (mod(FrameCount,2.) > 1.)
alpha += (Phase_Two-Phase_One)*2.;
#endif

// 1/size.x of screen in uv coords = InputSize.x/TextureSize.x/size.x;
// then 1/10*size.x of screen:
float ustep = InputSize.x/TextureSize.x/size.x/10.;

float border = InputSize.x/TextureSize.x;
float ss = 2.0;
#ifdef SWAP_VSIGN
#define PAL_SWITCH(A) A < 1.
#else
#define PAL_SWITCH(A) A > 1.
#endif
if (PAL_SWITCH(fmod(uv.y*TextureSize.y/InputSize.y*size.y,2.0)))
{
// cos(pi-alpha) = -cos(alpha)
// sin(pi-alpha) = sin(alpha)
// pi - alpha
alpha = -alpha+12012.0;
ss = -2.0;
}

float ysum = 0., usum = 0., vsum = 0.;
for (int i=0; i<int(Mwidth); ++i)
{
float4 res = texture2D(tex, uv);
#ifdef USE_RAW
float sig = NTSCsignal(res.xyz,HueShift*2.+alpha-res.g*ss*HueRotation)-Voltage_0;
// outside of texture is 0,0,0 which is white instead of black
if (uv.x <= 0.0 || uv.x >= border)
sig = 0.;
#ifdef USE_DELAY_LINE
float4 res1 = texture2D(tex, uv+sh);
float sig1 = NTSCsignal(res1.xyz,HueShift*2.+12012.0-alpha+res.g*ss*HueRotation)-Voltage_0;
if (uv.x + sh.x <= 0.0 || uv.x + sh.x >= border)
sig1 = 0.;
#endif

#else
float3 yuv = mul(RGB_to_YUV, res.xyz);
float a1 = alpha+(HueShift+2.5)*2.-yuv.x*ss*HueRotation;
float sig = yuv.x+dot(yuv.yz,sign(float2(sinn(a1),coss(a1))));
#ifdef USE_DELAY_LINE
float4 res1 = texture2D(tex, uv+sh);
float3 yuv1 = mul(RGB_to_YUV, res1.xyz);
float a2 = (HueShift+2.5)*2.+12012.0-alpha+yuv.x*ss*HueRotation;
float sig1 = yuv1.x+dot(yuv1.yz,sign(float2(sinn(a2),coss(a2))));
#endif

#endif
if (i < int(Ywidth))
ysum += sig;

#ifdef USE_DELAY_LINE
if (i < int(Uwidth))
usum += (sig+sig1)*sinn(alpha);
if (i < int(Vwidth))
vsum += (sig-sig1)*coss(alpha);
#else
if (i < int(Uwidth))
usum += sig*sinn(alpha);
if (i < int(Vwidth))
vsum += sig*coss(alpha);
#endif
alpha -= ss;
uv.x -= ustep;
}

#ifdef PARAMETER_UNIFORM
ysum *= Contrast/Ywidth;
usum *= Contrast*Saturation/Uwidth;
vsum *= Contrast*Saturation/Vwidth;
#endif

float3 rgb = mul(float3(ysum+Brightness*float(Ywidth_static),usum,vsum), YUV_to_RGB);
#if defined(USE_GAMMA) && !defined(USE_COLORIMETRY)
float3 rgb1 = saturate(rgb);
rgb = pow(rgb1, float3(Gamma/2.2,Gamma/2.2,Gamma/2.2));
#endif

#ifdef USE_COLORIMETRY
float3 rgb1 = saturate(rgb);
rgb = pow(rgb1, float3(Gamma,Gamma,Gamma));
#endif

#if (defined(USE_SUBPIXELS) || defined(USE_SCANLINES))
float2 q = (p*TextureSize/InputSize)*float2(TV_Pixels*3.,size.y*2.);
#endif

#ifdef USE_SCANLINES
float scanlines = size.y/OutputSize.x;
float top = fmod(q.y-0.5*scanlines*2.,2.);
float bottom = top+frac(scanlines)*2.;
float2 sw = saturate(min(float2(1.,2.),float2(bottom, bottom))
-max(float2(0.,1.),float2(top)))
+saturate(min(float2(3.,4.),float2(bottom, bottom))
-max(float2(2.,3.),float2(top)))
+floor(scanlines);
#ifdef ANIMATE_SCANLINE
#define SCANLINE_MUL (fmod(float(FrameCount),2.0)<1.0001 \
? sw.x*dark_scanline+sw.y \
: sw.x+sw.y*dark_scanline)
#else
#define SCANLINE_MUL (sw.x*dark_scanline+sw.y)
#endif
rgb = rgb*SCANLINE_MUL/(sw.x+sw.y);

/*
//old stupid method
float z =
#ifdef ANIMATE_SCANLINE
fmod(FrameCount,2.0)+
#endif
0.5;

if (abs(fmod(q.y+0.5,2)-z)<0.5)
rgb *= dark_scanline;
*/
#endif

// size of pixel screen in texture coords:
//float output_pixel_size = InputSize.x/(OutputSize.x*TextureSize.x);

// correctness check
//if (fmod(p.x*output_pixel_size,2.0) < 1.0)
//rgb = float3(0.,0.,0.);

#ifdef USE_SUBPIXELS
float pixels = TV_Pixels/OutputSize.x;
float left = fmod(q.x-0.5*pixels*3.,3.);
float right = left+frac(pixels)*3.;
float3 w = saturate(min(float3(1.,2.,3.),float3(right,right,right))
-max(float3(0.,1.,2.),float3(left,left,left)))
+saturate(min(float3(4.,5.,6.),float3(right,right,right))
-max(float3(3.,4.,5.),float3(left,left,left)))
+floor(pixels);
rgb = rgb*3.*w/(w.x+w.y+w.z);
#endif

#ifdef USE_COLORIMETRY
float3 xyz1 = mul(RGB_to_XYZ,rgb);
float3 srgb = saturate(mul(XYZ_to_sRGB,xyz1));
float3 a1 = 12.92*srgb;
float3 a2 = 1.055*pow(srgb,float3(1./2.4,1./2.4,1./2.4))-0.055;
float3 ssrgb;
   ssrgb.x = (srgb.x<0.0031308?a1.x:a2.x);
   ssrgb.y = (srgb.y<0.0031308?a1.y:a2.y);
   ssrgb.z = (srgb.z<0.0031308?a1.z:a2.z);
return ssrgb;
#else
return rgb;
#endif
}

// pos (left corner, sample size)
float4 monitor_sample(sampler2D tex, float2 p, float2 sample_)
{
// linear interpolation was...
// now other thing.
// http://imgur.com/m8Z8trV
// AT LAST IT WORKS!!!!
// going to check in retroarch...
float2 mysize = TextureSize;
float2 next = float2(0.25,1.0)/mysize;
float2 f = frac(float2(4.,1.)*mysize*p);
sample_ *= float2(4.,1.)*mysize;
float2 l;
float2 r;
if (f.x+sample_.x < 1.)
{
l.x = f.x+sample_.x;
r.x = 0.;
}
else
{
l.x = 1.-f.x;
r.x = min(1.,f.x+sample_.x-1.);
}
if (f.y+sample_.y < 1.)
{
l.y = f.y+sample_.y;
r.y = 0.;
}
else
{
l.y = 1.-f.y;
r.y = min(1.,f.y+sample_.y-1.);
}
float3 top = mix(monitor(tex, p), monitor(tex, p+float2(next.x,0.)), r.x/(l.x+r.x));
float3 bottom = mix(monitor(tex, p+float2(0.,next.y)), monitor(tex, p+next), r.x/(l.x+r.x));
return float4(mix(top,bottom, r.y/(l.y+r.y)),1.0);
}

void main()
{
#ifdef USE_SAMPLED
gl_FragColor = monitor_sample(Texture, v_vPos.xy, 1./OutputSize);
#else
gl_FragColor = float4(monitor(gm_BaseTexture, v_vTexcoord.xy), 1.0);
#endif
}
